/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint new-cap: 0 */
'use strict';

require('newrelic');

const bodyParser = require('body-parser');
const compression = require('compression');
const DDPServer = require('@sonar/sonar-ddp-handler');
const express = require('express');
const http = require('http');
const winston = require('winston');

const LeadsRetriever = require('./helpers/leads-retriever');

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

const router = express.Router();
const app = express();

app.set('port', process.env.PORT || '3000');
app.use(bodyParser.json());
app.use(compression());
app.use('/', router);

/**
 * HTTP handlers
 */
router.post('/yandex', (req, res) => {
  const getLeads = new LeadsRetriever(req.body);
  getLeads.grab()
    .then(data => res.status(200).json(data))
    .catch(error => {
      const errorBody = { error: { code: 1, message: error.message } };

      if (error && error.message === 'YANDEX ban') {
        errorBody.error.code = 2;
      }
      res.status(500).json(errorBody);
    });
});

/**
 * DDP methods
 */
app.server = http.createServer(app);
const server = new DDPServer({ httpServer: app.server });

server.methods({
  yandex: function yandex(req) {
    return new Promise((resolve) => {
      const getLeads = new LeadsRetriever(req);
      getLeads.grab()
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          const errorBody = { error: { code: 1, message: error.message } };

          if (error && error.message === 'Yandex ban') {
            errorBody.error.code = 2;
          }
          resolve(errorBody);
        });
    });
  },
});

app.server.listen(app.get('port'), () => {
  logger.info(`YANDEX SOOTHSAYER is up & running on port #${app.get('port')}`);
});

module.exports = app;
