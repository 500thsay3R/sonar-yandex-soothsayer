/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
/* eslint new-cap: 0 */
'use strict';

const _ = require('lodash');
const EnhancedSet = require('./enhanced-set');
const YandexSearch = require('@sonar/sonar-yandex-parser');
const mongoose = require('mongoose');
const Page = require('../models/page');
const PagesAnalysis = require('@sonar/sonar-webpage-analyzer');
const request = require('request');
const winston = require('winston');

mongoose.connect(process.env.MONGO_HOST);

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

/** Class representing leads extraction process. */
class LeadsRetriever {
  /**
   * Sets up a new instance of extraction process.
   * @param {Object} parameters
   * @param {String} parameters.query - The primary search key.
   */
  constructor(parameters) {
    this._links = new Set();
    this._parameters = parameters;
    this._results = new EnhancedSet();
  }

  /**
   * Get links to process.
   * @returns {Set}
   */
  get links() {
    return this._links;
  }

  /**
   * Get extracted leads.
   * @returns {EnhancedSet}
   */
  get results() {
    return this._results;
  }

  /**
   * Get an array of leads.
   * @returns {Array}
   */
  get leads() {
    return [...this._results];
  }

  /**
   * Get search parameters.
   * @returns {{query: String}|*}
   */
  get parameters() {
    return this._parameters;
  }

  /**
   * Convert the DB item to response item.
   * @param {Object} dbItem - An item extracted from the storage.
   * @returns {Promise}
   * @private
   */
  _formResult(dbItem) {
    const query = _.find(dbItem.queries, elem => {
      return elem.query === this._parameters.query;
    });

    const result = {
      link: dbItem.link,
      postType: dbItem.postType,
      channel: dbItem.channel,
      author: dbItem.author,
      title: dbItem.title,
      text: dbItem.text,
      index: query.index,
      date: dbItem.date,
      language: dbItem.language,
      location: dbItem.location,
      info: dbItem.info,
      media: dbItem.media,
      xframe: dbItem.xframe,
      potentialRate: query.potency,
      accuracyRate: query.accuracy,
    };

    this.results.bulkAdd(result);
    return Promise.resolve();
  }

  /**
   * Check if the given page has already been cached.
   * @param {Object} page - Some page to test.
   * @param {String} page.url - URL of the page to be tested.
   * @returns {Promise}
   * @private
   */
  _checkDB(page) {
    const dbQuery = Page
      .findOne({ link: page.url })
      .where('queries.query').equals(this.parameters.query)
      .exec();

    return dbQuery
      .then(item => {
        if (item) {
          this.links.delete(page.url);
          return this._formResult(item);
        }
        return null;
      })
      .catch(error => {
        logger.error(`@CHECK-DB :: Error on DB retrieval -> ${error}`);
      });
  }

  /**
   * Update an existing DB item.
   * @param {Object} item - The webpage whose cached version is to be updated.
   * @param {String} query - The search key.
   * @returns {Promise}
   * @private
   */
  _update(item, query) {
    return Page.update(
      { link: item.link },
      { $push: { queries:
        { query, index: item.index, accuracy: item.accuracyRate, potency: item.potentialRate },
      } }
    )
      .then(() => {
        logger.debug(`@UPDATE-DB :: Successfully updated database -> [${item.url}]`);
      })
      .catch(error => {
        logger.warn(`@UPDATE-DB :: DB update failure -> ${error.message}`);
      });
  }

  /**
   * Cache webpage info.
   * @param {Object} item - The webpage to be cached.
   * @param {String} query - The search key.
   * @returns {Promise}
   * @private
   */
  _cacheItem(item, query) {
    return new Promise((resolve, reject) => {
      const body = {
        link: item.link,
        channel: item.channel,
        author: item.author,
        title: item.title,
        text: item.text,
        date: item.date,
        language: item.language,
        location: item.location,
        info: item.info,
        media: item.media,
        xframe: item.xframe,
        comments: item.comments,
        queries: [
          { query, index: item.index, accuracy: item.accuracyRate, potency: item.potentialRate },
        ],
      };

      const pageToSave = Page(body);

      pageToSave.save(pageToSave)
        .then(() => {
          resolve();
        })
        .catch(error => {
          if (error && _.startsWith(error.message, 'E11000 duplicate key error index')) {
            logger.warn(`@CACHE-ITEM :: Dup error intervened -> ${item.link}`);
            resolve(this._update(item, query));
          } else {
            logger.warn(`@CACHE-ITEM :: Cache Failure -> ${error.message}`);
            logger.info(`ON ${JSON.stringify(item)}`);
            reject(new Error('DB failure'));
          }
        });
    });
  }

  /**
   * Perform bulk webpages caching.
   * @param {Object[]} items - The webpages to be cached.
   * @param {string} query - The search key.
   * @returns {Promise}
   * @private
   */
  _cacheItems(items, query) {
    const tasks = _.map(items, i => this._cacheItem(i, query));
    return Promise.all(tasks);
  }

  /**
   * Perform page fetch and content analysis.
   * @param {Object} parameters - Content analysis parameters.
   * @returns {Promise}
   * @private
   */
  _fetch(parameters) {
    const analyses = new PagesAnalysis(parameters);
    return analyses.leadsScan()
      .then(data => {
        this.results.bulkAdd(data);
        return this._cacheItems(data, parameters.query);
      });
  }

  /**
   * Process pages extracted from Yandex Search results.
   * @param {Object} pages - Pages to be tested.
   * @param {Object} parameters - The search parameters.
   * @returns {Promise}
   * @private
   */
  _processPages(pages, parameters) {
    const tasks = _.map(pages, p => this._checkDB(p));

    return Promise.all(tasks)
      .then(() => {
        const links = [...this._links];

        if (links.length) {
          const toFetch = _.filter(pages, p => links.indexOf(p.url) !== -1);
          parameters.pages = toFetch;
          logger.info(`@PAGE-ANALYSIS :: ${toFetch.length} new pages to be fetched.`);
          return this._fetch(parameters);
        }
        return null;
      });
  }

  /**
   * Reboot this instance.
   * @param {number} tries - Tries left.
   * @returns {Promise}
   * @private
   */
  _reboot(tries = 3) {
    return new Promise((resolve, reject) => {
      request({ url: process.env.REWAKER_URL, method: 'GET' }, (error) => {
        if (error && tries <= 0) {
          logger.error(`@PAGE-ANALYSIS :: Failed to reboot YANDEX SOOTHSAYER -> [${error}]`);
          reject(new Error('Yandex ban'));
        } else {
          resolve(this._reboot(--tries));
        }
      });
    });
  }

  /**
   * Perform leads extraction.
   * @returns {Promise}
   */
  grab() {
    return new Promise((resolve, reject) => {
      const scan = new YandexSearch(this.parameters);

      scan.find()
        .then((pages) => {
          _.forEach(pages, p => this._links.add(p.url));
          return this._processPages(pages, this.parameters);
        })
        .then(() => {
          resolve(this.leads);
        })
        .catch(error => {
          logger.error(`@PAGE-ANALYSIS :: Internal error -> ${error.message || error}`);

          if (error.message === 'Yandex ban') {
            resolve(this._reboot());
          }
          reject(new Error(error.message || 'Internal error'));
        });
    });
  }
}

module.exports = LeadsRetriever;
