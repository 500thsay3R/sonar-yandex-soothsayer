/* eslint-env: node, es6 */
/* eslint strict: 0 */
'use strict';

class EnhancedSet extends Set {
  /**
   * Implement bulk elements adding to Set instance.
   * @param {*} items - The items to add.
   */
  bulkAdd(items) {
    if (Array.isArray(items)) {
      items.forEach(item => super.add(item));
    } else {
      super.add(items);
    }
  }
}

module.exports = EnhancedSet;
