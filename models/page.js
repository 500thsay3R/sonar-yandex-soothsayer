/* eslint-env: node, es6 */
/* eslint strict: 0 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Query = new Schema({
  query: { type: String, required: true, lowecase: true },
  index: { type: Number, required: true },
  accuracy: { type: Number, required: true, min: 11, max: 100 },
  potency: { type: Number, required: true, min: 11, max: 100 },
});

const pageSchema = new Schema({
  link: { type: String, required: true, unique: true, index: true },
  postType: { type: String, required: true, default: 'webpage' },
  author: { type: String },
  title: { type: String, required: true },
  text: { type: String },
  queries: [Query],
  date: { type: Date },
  language: { type: String, required: true, match: /[a-z]{2}/ },
  location: { type: String },
  info: {
    rank: { type: Number },
  },
  media: {
    hashtags: [String],
  },
  xframe: { type: Boolean, required: true },
  comments: { type: [String], required: true },
  createdAt: { type: Date, default: Date.now, required: true, expires: 604800000 },
});

const Page = mongoose.model('Page', pageSchema);

module.exports = Page;
